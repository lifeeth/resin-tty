apt-get update
apt-get install -y avahi-daemon nano

echo 'echo "resin" > /etc/hostname' > /usr/bin/localdns.start
echo '/etc/init.d/hostname.sh' >> /usr/bin/localdns.start
echo '/etc/init.d/dbus start' >> /usr/bin/localdns.start
echo '/etc/init.d/avahi-daemon start && sleep 10s' >> /usr/bin/localdns.start

chmod a+x /usr/bin/localdns.start


var tty = require('tty.js');
// Simple tty.js in app mode
var app = tty.createServer({
  shell: 'bash',
  users: {
    admin: 'admin'
  },
  port: process.env.PORT || '8080'
});

Tail = require('tail').Tail;

var fs = require('fs'),
          spawn = require('child_process').spawn,
	             out = fs.openSync('./out.log', 'a'),
		                     err = fs.openSync('./out_err.log', 'a');


var child = spawn('/usr/bin/localdns.start', [], {
            detached: true,
            stdio: [ 'ignore', out, err ]
          });

tail = new Tail("./out.log");
tail.on("line", function(data) {
          console.log(data);
});

child.unref();

app.listen();
